const state = {
    fps: 15,
    color: "#0f0",
    charset: "0123456789ABCDEF",
    density: 1
};

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

let w, h, p;
const resize = () => {
    w = canvas.width = innerWidth;
    h = canvas.height = innerHeight;
    p = Array(Math.ceil(w / state.density)).fill(0);
    //console.log(`${w}:${h}`);
};
window.addEventListener("resize", resize);
resize();

const random = (items) => items[Math.floor(Math.random() * items.length)];

const draw = () => {
    ctx.fillStyle = "rgba(0,0,0,.05)";
    ctx.fillRect(0, 0, w, h);
    ctx.fillStyle = state.color;

    for (let i = 0; i < p.length; i++) {
        let v = p[i];
        ctx.fillText(random(state.charset), i * state.density, v);
        p[i] = v >= h || v >= 10000 * Math.random() ? 0 : v + 10;
    }
};

let interval = setInterval(draw, 1000 / state.fps);
